﻿using System.Configuration;
using TelegramBot.Core.Consts;

namespace TelegramBot.Core.Helpers
{
    internal class ConfigHelper
    {
        public static string? Token { get => GetValue(ConfigKeys.Token); }
        public static string? ToChatId { get => GetValue(ConfigKeys.ToChatId); }
        public static string? FromChatId { get => GetValue(ConfigKeys.FromChatId); }

        static string? GetValue(string key) => ConfigurationManager.AppSettings[key];

        public static bool CheckConfig()
        {
            bool token = Token != null;

            if (!token) { Console.WriteLine($"\nMissing config: {ConfigKeys.Token}"); }

            bool toChatId = ToChatId != null;

            if (!toChatId) { Console.WriteLine($"\nMissing config: {ConfigKeys.ToChatId}"); }

            bool fromChatId = FromChatId != null;

            if (!fromChatId) { Console.WriteLine($"\nMissing config: {ConfigKeys.FromChatId}"); }

            if (token && toChatId && fromChatId)
            {
                Console.WriteLine($"\nConfig: OK");
                return true;
            }

            return false;
        }
    }
}
