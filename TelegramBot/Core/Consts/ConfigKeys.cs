﻿namespace TelegramBot.Core.Consts
{
    internal static class ConfigKeys
    {
        public const string Token = "token";

        public const string ToChatId = "to_chat_id";

        public const string FromChatId = "from_chat_id";
    }
}
