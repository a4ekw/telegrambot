﻿namespace TelegramBot.Core.Consts
{
    internal static class Tags
    {
        public const string Question = "#question";

        public static List<string> List => new() { Question };
    }
}