﻿namespace TelegramBot.Core.Interfaces
{
    interface IUseCaseIn<in T>
    {
        void Execute(T param);
    }
    interface IUseCaseOut<out Task>
    {
        Task Execute();
    }

    interface IUseCaseInOut<in T, out Task>
    {
        Task Execute(T param);
    }
}
