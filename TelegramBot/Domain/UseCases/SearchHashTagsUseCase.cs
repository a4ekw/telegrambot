﻿using Microsoft.Extensions.DependencyInjection;
using System.Text.RegularExpressions;
using Telegram.Bot.Types;
using TelegramBot.Core.Consts;
using TelegramBot.Core.Interfaces;
using TelegramBot.DI;

namespace TelegramBot.Domain.UseCases
{
    internal class SearchHashTagUseCase : IUseCaseIn<Message>
    {
        public void Execute(Message param)
        {
            var text = param.Text ?? "";

            if (text.Length != 0)
            {
                var list = text.Split(" ");

                foreach (string tag in Tags.List)
                {
                    if (list.Contains(tag))
                    {

                        Console.WriteLine($"\nTag: {tag}");

                        switch (tag)
                        {
                            case Tags.Question:
                            default:
                                try
                                {
                                    Injections.provider?.GetRequiredService<ForwardUseCase>().Execute(param.MessageId);
                                }

                                catch
                                {
                                    Console.WriteLine("\nForwardUseCase not found!");
                                }

                                break;
                        }
                    }
                }
            }
        }
    }
}
