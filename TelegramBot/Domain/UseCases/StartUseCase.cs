﻿using Telegram.Bot;
using Telegram.Bot.Polling;
using Telegram.Bot.Types.Enums;
using TelegramBot.Core.Helpers;
using TelegramBot.Core.Interfaces;
using TelegramBot.handlers;


namespace TelegramBot.Domain.UseCases
{
    internal class StartUseCase : IUseCaseOut<ITelegramBotClient>
    {
        readonly CancellationTokenSource tokenSource;

        public StartUseCase(CancellationTokenSource tokenSource)
        {
            this.tokenSource = tokenSource;
        }

        public ITelegramBotClient Execute()
        {
            ITelegramBotClient bot = new TelegramBotClient(ConfigHelper.Token!);

            bot.StartReceiving(
                BotHandlers.HandleUpdateAsync,
                BotHandlers.HandleErrorAsync,
                new ReceiverOptions
                {
                    AllowedUpdates = Array.Empty<UpdateType>(),
                },
                tokenSource.Token
            );
            try
            {
                Console.WriteLine("\nExecution: " + bot.GetMeAsync().Result.FirstName);
            } 
            
            catch
            {
                Console.WriteLine("\nData could not be retrieved!\nCheck the token");
            }

            return bot;
        }
    }
}