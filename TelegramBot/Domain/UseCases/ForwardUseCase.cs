﻿using Telegram.Bot;
using TelegramBot.Core.Interfaces;


namespace TelegramBot.Domain.UseCases
{
    internal class ForwardUseCase : IUseCaseIn<int>
    {
        readonly ITelegramBotClient bot;

        readonly string fromChatId, toChatId;

        public ForwardUseCase(ITelegramBotClient bot, string fromChatId, string toChatId)
        {
            this.bot = bot;
            this.fromChatId = fromChatId;
            this.toChatId = toChatId;
        }

        public async void Execute(int param)
        {

            try
            {
                await bot.ForwardMessageAsync(
                messageId: param,
                fromChatId: fromChatId,
                chatId: toChatId
                );

                Console.WriteLine("\nThe message has been forwarded");
            }

            catch
            {
                Console.WriteLine($"\nFailed to forward the message: {param}\n" +
                    $"From: {fromChatId}\n" +
                    $"To: {toChatId}\n");
            }
        }
    }
}