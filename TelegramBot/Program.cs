﻿using Microsoft.Extensions.DependencyInjection;
using TelegramBot.Core.Helpers;
using TelegramBot.DI;
using TelegramBot.Domain.UseCases;

namespace TelegramBot
{
    internal class Program
    {
        static void Main()
        {
            if (ConfigHelper.CheckConfig())
            {

                Injections.Init();

                var startUseCase = Injections.provider?.GetRequiredService<StartUseCase>();

                if (startUseCase != null)
                {
                    var bot = startUseCase.Execute();

                    if (bot != null)
                    {
                        Injections.services.AddSingleton(
                            new ForwardUseCase(
                                bot: bot,
                                fromChatId: ConfigHelper.FromChatId!,
                                toChatId: ConfigHelper.ToChatId!
                                )
                            );

                        Injections.services.AddSingleton(new SearchHashTagUseCase());

                        Injections.provider = Injections.services.BuildServiceProvider();

                        Console.ReadLine();

                        Injections.provider?.GetRequiredService<CancellationTokenSource>()?.Cancel();
                    }

                    else
                    {
                        Console.WriteLine("Failed to launch the bot\n");
                        Console.ReadLine();
                    }
                }

                else
                {
                    Console.WriteLine("Injections error\n");
                    Console.ReadLine();
                }
            }

            else
            {
                Console.ReadLine();
            }
        }
    }
}