﻿using Microsoft.Extensions.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramBot.DI;
using TelegramBot.Domain.UseCases;

namespace TelegramBot.handlers
{
    internal class BotHandlers
    {
        public static Task HandleUpdateAsync(ITelegramBotClient _, Update update, CancellationToken __)
        {
            Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(update));

            if (update.Type == Telegram.Bot.Types.Enums.UpdateType.Message)
            {
                if (update.Message?.MessageId != null)
                {
                    try
                    {
                        var searchHashTagUseCase = Injections.provider?.GetRequiredService<SearchHashTagUseCase>();
                        searchHashTagUseCase!.Execute(update.Message!);
                    }
                    catch
                    {
                        Console.WriteLine("SearchHashTagUseCase not found!");
                    }
                }
            }

            return Task.CompletedTask;
        }

        public static Task HandleErrorAsync(ITelegramBotClient _, Exception exception, CancellationToken __)
        {
            Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(exception));
            return Task.CompletedTask;
        }
    }
}