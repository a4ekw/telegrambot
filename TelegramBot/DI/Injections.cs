﻿using Microsoft.Extensions.DependencyInjection;
using TelegramBot.Domain.UseCases;

namespace TelegramBot.DI
{
    internal static class Injections
    {
        public static IServiceCollection services = new ServiceCollection();

        public static IServiceProvider? provider;

        public static void Init()
        {
            services.AddSingleton(new CancellationTokenSource());
            provider = services.BuildServiceProvider();

            services.AddSingleton(
                new StartUseCase(tokenSource: provider.GetService<CancellationTokenSource>()!)
                );

            provider = services.BuildServiceProvider();
        }
    }
}