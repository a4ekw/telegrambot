# TelegramBot

Forwards messages from one chat to another provided: that the message is marked with the #question tag.

## Getting started

Configure the parameters of the telegram bot in the app.config file:

token - your bot's token;

from_chat_id - Id of the chat to listen to messages in;

to_chat_id - Id of the chat to forward messages to;

## Make sure that you have specified the correct token and group ID, added the bot to the appropriate groups, and configured the bot policy for accessing private messages!